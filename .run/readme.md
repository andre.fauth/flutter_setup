#AndroidStudioSetUp
Note, AS and IntelliJ have switched to storing run configurations in a .run sub-folder but Android Studio keeps runconfigs in the old legacy .idea folder location. Thus, I copy them here to conform to the new way. 

Same basics as VSCode in that you slim down to only use plugins needed to slim down memory footprint to deal with doing flutter development on cheap laptops. Details on this an efficient flutter IDE usage can be found in the article referenced in resources.

Editor settings are found in the settings sub-folder as the settings.zip file.

#Plugins
Flutter
Dart
Flutter Pub Versin Checker
GenyMotion
Markdown
PlantUML Integration


#Resources
[Expert Android Studio Flutter SetUp]()