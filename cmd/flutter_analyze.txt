Analyze the project's Dart code.

Global options:
-h, --help                  Print this usage information.
-v, --verbose               Noisy logging, including all shell commands executed.
                            If used with --help, shows hidden options.
-d, --device-id             Target device id or name (prefixes allowed).
    --version               Reports the version of this tool.
    --suppress-analytics    Suppress analytics reporting when this command runs.

Usage: flutter analyze [arguments]
-h, --help                    Print this usage information.
    --[no-]current-package    Analyze the current project, if applicable.
                              (defaults to on)
    --watch                   Run analysis continuously, watching the filesystem for changes.
    --write=<file>            Also output the results to a file. This is useful with --watch if you want a file to always contain the latest results.
    --[no-]pub                Whether to run "flutter pub get" before executing this command.
                              (defaults to on)
    --[no-]congratulate       Show output even when there are no errors, warnings, hints, or lints. Ignored if --watch is specified.
                              (defaults to on)
    --[no-]preamble           When analyzing the flutter repository, display the number of files that will be analyzed.
                              Ignored if --watch is specified.
                              (defaults to on)
    --[no-]fatal-infos        Treat info level issues as fatal.
                              (defaults to on)
    --[no-]fatal-warnings     Treat warning level issues as fatal.
                              (defaults to on)

Run "flutter help" to see global options.
