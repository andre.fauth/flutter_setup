import 'package:flutter/material.dart';
import 'package:flutter_setup/presentation/my_app.dart';

// In Domain Driven Development terms you would move the other
// widget presentation classes to the presentation sub-folder
void main() {

  runApp(MyApp());
}


