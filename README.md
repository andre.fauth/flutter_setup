<!-- PROJECT LOGO  should always be somewhat the twitter card size-->
![gitlab repo card](./images/expert-flutter-setup.png)


## flutter_setup
<!-- Description should be a story in sentence fragment form describing the project's value 
proposition to the developer, designer, etc.The general story form is of a short joke. 
Namely the struggle, the discovery, and the surprise. I found I could not do AAA until 
I shot Buddha and this Project helps you Shoot Buddha.-->

A Flutter Project Template that you can copy,paste, and modify to have all the feed-back right from 
the very first day of you beginning to learn flutter and dart. Features widget testing, integration
testing, api doc generation, flexible workflow automation via the derry dart plugin, and uml diagram 
generation.




<!-- PROJECT SHIELDS gitlab has not changed how they integrate with shields.io yet doing to auth tokens but we still can
     read the information  we need without an auth token as of Jan 2021
     replace `https://gitlab.com/api/v4/projects/23873593` and use onw gitlab project ID .-->
![License](https://img.shields.io/badge/dynamic/json.svg?label=License&url=https://gitlab.com/api/v4/projects/24567407?license=true&query=license.name&colorB=yellow)
![Created at](https://img.shields.io/badge/dynamic/json.svg?label=Created%20at&url=https://gitlab.com/api/v4/projects/24567407&query=created_at&colorB=informational)
![Last activity](https://img.shields.io/badge/dynamic/json.svg?label=Last%20activity&url=https://gitlab.com/api/v4/projects/24567407&query=last_activity_at&colorB=informational)

<!-- TABLE OF CONTENTS gitlab does auto via their GFM one needs to add a toc tag on its own line-->

# Table of contents
1. [About The Project](#About The Project)
   1. [Built With](#Built With)
2. [Getting Started](#Getting Started)
   1. [Prerequisites](#Prerequisites)
   2. [Installation](#Installation)
3. [Usage](#Usage)
4. [Roadmap](#Roadmap)
5. [Contributing](#Contributing)
6. [Contributors](#Contributors)
7. [License](#License)
8. [Contact](#Contact)
9. [Acknowledgements](#Acknowledgements)
10.[Flutter Community Resources](#Flutter Community Resources)
<!-- ABOUT THE PROJECT -->
## About The Project

<!-- Always wrap to center as you may have more than one screenshot to show.The length of table-of-contents determines whether the screenshot gets placed here or after the project shields as we want at least parth of the screenshots above the fold in the dekstop display of the webpage generated from the markdown.-->
![screenshot](./images/screenshot.png)

Picture this, you want to learn flutter and create that great flutter mobile app in your head. The 
problem is you just use the project template that flutter creates when you create a flutter project 
in  your favorite IDE than you do not get the immediate feedback from unit testing, integration 
testing, etc that you need to steer yourself from blind-spots in self learning flutter.

Think of this project as a small set of training wheels to keep you on the path of building that 
beautiful flutter app. Details about the setup as far as tool dependencies and sdk's setup are detailed
in the [setup.md](SETUP.md). They are also oovered in a medium article:

https://fredgrott.medium.com/flutter-perfect-setup-c5462b412f78


_WIP_: Note that Integration-testing is migrating away from using the flutter driver package. 
Right now, march 2021, we still use some flutter driver features to drive the app being tested. 
Also note that we do not yet have a json or reporter flag for an easier formof test output to 
transform to CI and human readable content.

### Built With

* [Android Studio](https://developer.android.com/studio)
* [Flutter](https://flutter.dev/)
* [Dart](https://dart.dev/)

<!-- GETTING STARTED -->
## Getting Started

To get a local copy up and running follow these simple steps.

### Prerequisites

Most front-end application code requires usage with the git tool. To install git:

* [How To Install Git](https://git-scm.com/)

For Flutter Mobile Applications you need to install at least one mobile SDK:

* [How To Install Android SDK](https://anddroid.dev)
* [How To Install iOS SDK on MACOSX install XCope to get it](https://apps.apple.com/us/app/xcode/id497799835?mt=12)

Than install the Flutter SDK

* [How To Insall  the Flutter SDK](https://flutter.dev/docs/get-started/install.html)

Than install an IDE

* [How To install the Android IDE](https://developer.android.com/studio)
* [How To install the MS VSCode IDE](https://code.visualstudio.com/)


### Installation
1.  Clone the repo via _sh_

        git clone https://gitlab.com/fred.grott/flutter_setup.git


2.  Open the project in your Android Studio or MS VSCode IDE


<!-- USAGE EXAMPLES -->
## Usage





<!-- ROADMAP -->
## Roadmap



<!-- CONTRIBUTING -->
## Contributing

Not taking contributions at this time but you are free to fork it and modify it.

<!-- CONTRIBUTORS-->
### Contributors




<!-- LICENSE -->
## License

Distributed under the BSD License. See [LICENSE](https://gitlab.com/fred.grott/flutter_setup/-/blob/master/LICENSE) for more information.



<!-- CONTACT -->
## Contact
<!-- email addy is always stated in an expanded way as it obfuscates it from picked up by web scrapers and generating spam emails-->
Fred Grott - [@twitter_handle](https://twitter.com/fredgrott) - email: fred DOT grott AT gmail DOT com

Project Link: [https://gitlab.com/fred.grott/flutter_setup](https://gitlab.com/fred.grott/flutte_setup)



<!-- ACKNOWLEDGEMENTS -->
## Acknowledgements

I am going to give a shout-out to someone I have never met, Zed Shaw. For showing me the way to create 
developer training that has meaning and depth. Hopefully, I get to buy a drink sometime.


<!-- RESOURCES-->
<!-- You should put some resources to the computer language, frameworks, etc as it decreases someone opening issues of how to use that computer laguage or framework.-->
## Flutter Community Resources
* [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
* [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)
* [Flutter online documentation](https://flutter.dev/docs)
* [Dart online documentation](https://dart.dev/docs)
* [Ask Flutter Dev questions@Stackoverflow](https://stackoverflow.com/tags/flutter)
* [Ask Flutter Dev questions@Reddit](https://www.reddit.com/r/FlutterDev/)
* [Flutter Community Articles@Medium.com](https://medium.com/flutter-io)
* [Solido Awesome Flutter List Of Resources](https://github.com/Solido/awesome-flutter)
* [Flutter Dev Videos@Youtube](https://www.youtube.com/playlist?list=PLOU2XLYxmsIJ7dsVN4iRuA7BT8XHzGtCr)
* [Ask questions about Flutter@Hashnode](https://hashnode.com/n/flutter)
